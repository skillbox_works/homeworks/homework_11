// HomwWork_11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void print_text(int i, int r)
{
    std::cout << i;
    std::cout << " step = ";
    std::cout << r;
    std::cout << "\n";
}

int main()
{
    // std::cout << "Hello World!\n";
    for (int i = 0; i < 3; i++)
    { 
        print_text(i+1, rand());
    }

}
